package main

import (
	"time"
    "log"
    "fmt"
    "encoding/json"
    "github.com/tal-tech/go-zero/core/cmdline"
    "gitee.com/wkkcool/tech/go-queue/nq"
)

func main() {
    pusher := nq.NewPusher("192.168.6.178:4150", "auto")
	ticker := time.NewTicker(time.Millisecond)
	for round := 0; round < 3; round++ {
		select {
		case <-ticker.C:
			m := struct{
				Type int
				Data string
			}{
                Type:3,
                Data:"this is test",
            }
			body, err := json.Marshal(m)
			if err != nil {
				log.Fatal(err)
			}

			fmt.Println(string(body))
			if err := pusher.Push(string(body)); err != nil {
				log.Fatal(err)
			}
		}
	}

	cmdline.EnterToContinue()
}
