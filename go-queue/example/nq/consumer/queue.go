package main

import (
	"fmt"

	"gitee.com/wkkcool/tech/go-queue/nq"
	"github.com/tal-tech/go-zero/core/conf"
)

func main() {
	var c nq.NqConf
	conf.MustLoad("config.yaml", &c)
    // fmt.Printf("%q\n",c)

	q := nq.MustNewQueue(c, nq.WithHandle(func(k, v string) error {
		fmt.Printf("=> %s\n", v)
		return nil
	}))
	defer q.Stop()
	q.Start()
}
