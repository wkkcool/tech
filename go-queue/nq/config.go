package nq

import (
	"fmt"
)

const (
	firstOffset = "first"
	lastOffset  = "last"
)

type NqConf struct {
	// service.ServiceConf
	Brokers    []string //连接的nsqlookupd
	Group      string   //可默认为分区ID,标准版nsq无效,有赞版有用(类型为int,需要转换)
	Topic      string
	Offset     string `json:",options=first|last,default=last"` //标准版nsq无效,有赞版有用(从哪个位置读取)
	Conns      int    `json:",default=1"`                       //建立多少连接
	Consumers  int    `json:",default=1"`                       //1个连接，多个协程读取,nsq固定为1.
	Processors int    `json:",default=8"`                       //1个连接，多少协程去消费
	Channel    string `json:",default=default"`                 //消费的channel,默认为default
}

type MessageHandler struct {
}

func (mh *MessageHandler) Consume(key, val string) error {
	fmt.Printf("===========> key=[%q],val=[%q]\n", key, val)
	return nil
}
