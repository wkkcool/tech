package logx


const (
	MSG_DEFT MsgType = iota //默认日志格式
	MSG_SELF            //自定义格式，使用方需要自己封装日志内容
	MSG_JSON            //json日志格式
	numMsgType          //设置的类型不能超过numMsgType-1
)

type LogConf struct {
	ServiceName string `json:"name,optional"` //此日志服务名，默认为程序名。
	V         int  `json:"level,default=0"`     //此日志的全局等级，超过此等级的日志不显示和保存
	Vmodule   string `json:"vmodule,optional"`  //遵守全局V等级设置下,再按照文件设置等级,格式为 文件名=等级,此文件名不带后缀，比如 main=3
	Backtrace string `json:"backtrace,optional"`              //追踪日志,格式为文件名:行号,比如main.go:234，此文件名带后缀
	WriteConsole    bool `json:"console,default=true"` //是否打印屏幕

    //下面的为文件相关配置
	WriteFile   bool `json:"writefile,default=false"`   //是否保存文件
	Path        string `json:"path,default=logs"`   //保存文件的目录
	Compress  bool   `json:"compress,optional"`     //日志文件备份是否zip压缩
	KeepDays  int    `json:"keepdays,optional"`     //日志文件保存天数，0表示永久保存
	MaxSize  int    `json:"maxsize,default=8388608"` //单个日志文件的最大容量，超过此值就需要切换新文件，避免单个文件过大。默认8M。
	FlushTm  int       `json:"flushtm,default=5"` //多少秒刷新一次文件
	ContentType  int64       `json:"contenttype,default=0"` //输出日志的类型，0:默认格式 1:自定义格式 2:json格式
    TimeFormat      string `json:",optional"` //ContentType!=0时，自定义时间戳格式 类似"2006-01-02 15:04:05.000000"
}

func NewConfig() *LogConf {
	c := &LogConf{
		ServiceName: program,
		Path:        "logs",
        WriteConsole: true,
        MaxSize: 8388608*20,
        Compress: true,
        KeepDays: 30,
	}
	return c
}
